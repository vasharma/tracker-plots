title: '$Z^{0}$ peak mass as a function of the pseudorapidity and the azimuthal angle of the positive muon for 2011 and 2021 data'
caption: 'Distortions of the tracker geometry can lead to a bias in the reconstructed track curvature $\kappa \propto \pm 1/p_{T}$. 

These are investigated using the reconstructed $Z^{0}\rightarrow\mu\mu$ mass, as a function of the muon direction and separating $\mu^{+}$ and $\mu^{-}$ (since curvature bias has opposite effect on their $p_{T}$).
Invariant mass distribution is fitted to a Breit-Wigner convoluted with a Crystal ball function (i.e. taking into account the finite track resolution and the radiative tail) for the signal plus an exponential background. Fit range is 75-105 GeV/$c^{2}$, $Z^{0}$ width fixed to PDG value of 2.495 GeV/$c^{2}$.
This does not show the CMS muon reconstruction and calibration performance! It is the tracker input to all object reconstruction. Physics analyses apply momentum calibration on top of this. 

The plots shown are:

$\bullet$ $Z^{0}$ peak mass as a function of the pseudorapidity $\eta(\mu^{+})$ and the azimuthal angle $\phi(\mu^{+})$ of the positive muon for 2011 data. The z-axis range is centred at the peak value corresponding to the fitted mass for all events in the 2011 sample (91.08 !GeV/$c^{2}$)

$\bullet$ $Z^{0}$ peak mass as a function of the pseudorapidity $\eta(\mu^{+})$ and the azimuthal angle $\phi(\mu^{+})$ of the positive muon for 2012 data. The z-axis range is centred at the peak value corresponding to the fitted mass for all events in the 2011 sample (91.08 !GeV/$c^{2}$). Overall pattern significantly reduced for 2012 data.
'
date: '2013-05-15'
tags:
- Run-1
- pp
- Collisions
- 2011
- 2012
- Tracker
- Alignment