title: '$Z^{0}$ peak mass as a function of the azimuthal angle of the positive muon for barrel, backward and forward muons'
caption: 'Distortions of the tracker geometry can lead to a bias in the reconstructed track curvature $\kappa \propto \pm 1/p_{T}$. 

These are investigated using the reconstructed $Z\rightarrow\mu\mu$ mass, as a function of the muon direction and separating $\mu^{+}$ and $\mu^{-}$ (since curvature bias has opposite effect on their $p_{T}$).
Invariant mass distribution is fitted to a Breit-Wigner convoluted with a Crystal ball function (i.e. taking into account the finite track resolution and the radiative tail) for the signal plus an exponential background. Fit range is 75-105 GeV/$c^{2}$, $Z^{0}$ width fixed to PDG value of 2.495 GeV/$c^{2}$.
This does not show the CMS muon reconstruction and calibration performance! It is the tracker input to all object reconstruction. Physics analyses apply momentum calibration on top of this. 

The plots shown are:

$\bullet$ $Z^{0}$ peak mass as a function of the azimuthal angle of the positive muon $\phi(\mu^{+})$ for barrel muons only $(\mid \eta(\mu^{\pm}) \mid <0.9)$. Sensitive to distortions of the tracker in the transverse plane (e.g. the so-called "sagitta"). Amplitude of sine-wave like structure in barrel reduced from about 0.7 GeV/$c^{2}$ to about 0.3 GeV/$c^{2}$.

$\bullet$  $Z^{0}$ peak mass as a function of the azimuthal angle of the positive muon $\phi(\mu^{+})$ for all events in the backward ($\eta(\mu^{\pm}) < -0.9$) region. Slightly less regular pattern, but much flatter for 2012. 

$\bullet$ $Z^{0}$ peak mass as a function of the azimuthal angle of the positive muon $\phi(\mu^{+})$ for all events in the forward region ($\eta(\mu^{\pm}) > 0.9$). Slightly less regular pattern, but much flatter for 2012.
'
date: '2013-05-15'
tags:
- Run-1
- pp
- Collisions
- 2011
- 2012
- Tracker
- Alignment