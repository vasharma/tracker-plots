title: 'Difference of the mean of the distribution of median residuals in the local-$x$ ($x^\prime$) direction, for barrel pixel modules with electric field pointing radially inwards and outwards'
caption: 'In the barrel region, distribution of median residuals (DMR) can be obtained separately for the modules with electric field pointing radially inwards or outwards. Difference of their mean values $\Delta\mu$ in the local-$x$ ($x^\prime$) direction, which is orthogonal to the magnetic field, is an index of goodness in recovering Lorentz angle effects. Here, $\Delta\mu$ is shown for the pixel barrel modules as a function of integrated luminosity. The uncertainty corresponds to the square root of the quadratic sum of the uncertainties calculated separately for the inward and outward pointing modules. The vertical black solid lines indicate the first processed run for 2016, 2017 and 2018 data-taking period, respectively. The vertical dotted lines indicate a change in the tracker pixel calibration. The blue points correspond to the results with the alignment constants used during data-taking, the red points show the results with the alignment constants used during the 2016, 2017 and 2018 End-Of-Year (EOY) re-reconstruction (notice there is no EOY re-reconstruction for the last 33$fb^{-1}$ of data-taking), the green points show the results with the alignment constants as obtained in the Run-2 Legacy alignment procedure. After the dedicated Run-2 Legacy alignment, the mean difference shows improved stability.'

date: '2020-02-27'
tags:
- Run-2
- pp
- Collisions
- Tracker
- Alignment
- Phase-1 Pixel
- Time Evolution