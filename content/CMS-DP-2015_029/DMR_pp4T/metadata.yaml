title: 'Distribution of median residuals in different tracker subdetectors for the 2015 PCL-like alignment derived with 3.8T collision tracks'

caption: 'The tracker geometry changed again when the magnetic field was turned back on. This alignment was produced in an automated process, which will be used as part of the Prompt Calibration Loop (PCL), which activates as the data is collected and processed. Alignment is performed with a relatively small amount tracks (much less than the 20 million tracks used in the validation below), and the current alignment is used as a starting point. New alignment constants are fitted for larger substructures of the pixel detector (BPIX half-barrels and FPIX half-cylinders) only. The changes, again produced by the changing magnetic field, are recovered by this alignment.

Shown are the distributions of median residuals for the local x-direction in the barrel and forward pixel detectors, as well as for the following strip subdetectors: TIB, TID and TEC. For the barrel and forward pixel detectors, the distributions are also shown for the local y-direction. 

The blue line shows the Run I geometry, which is no longer valid for Run II data, primarily because of temperature changes and pixel re-centering and repair. The black line shows the starting geometry for data taking, which was valid for data taken with the magnetic field turned off, as it was produced with the Millepede-II and HIP algorithms using cosmic ray and 0T collision data. The alignment shown in violet was adjusted from this geometry by an automated alignment process of the pixel detector that will be run as part of the Prompt Calibration Loop as data is collected and processed, and shows improvements over the initial geometry. The changes resulted primarily from the change in the magnetic field.

For BPIX modules, in the local x-direction, the RMS of the distribution reduces from 118.9 and 9.4 to 3.3$\mu$m for the Run I, initial, and aligned geometry, respectively.
In the local y-direction, the RMS of the distribution reduces from 90.1 and 27.0 to 10.0$\mu$m for the Run I, initial, and aligned geometry, respectively. The double-peak structure present when assuming the initial geometry in the track refit is corrected by the PCL-style alignment of the pixel detector.

For FPIX modules, in the local x-direction, the RMS of the distribution reduces from 63.1 and 13.6 to 4.6$\mu$m for the Run I, initial, and aligned geometry, respectively. In the local-y direction, the RMS of the distribution reduces from 32.7 and 11.7 to 7.1$\mu$m for the Run I, initial, and aligned geometry, respectively.

The RMS of the distribution for TIB modules in the local-x direction reduces from 36.3 and 5.8 to 3.1$\mu$m for the Run I, initial, and aligned geometry, respectively. 

For TID modules, the RMS of the distribution reduces from 42.1 to 5.4 and 4.6$\mu$m for the Run I, initial, and aligned geometry, respectively.

For TEC modules, the RMS of the distribution reduces from 13.3 to 5.3 and 5.2$\mu$m for the Run I, initial, and aligned geometry, respectively. Here the improvement is less visible, as the changes in the geometry resulted primarily from the change in the magnetic field, and affected the inner detectors more than the outer detectors, such as the endcaps. 
'

date: '2015/08/10'

tags:
- Run-2
- Alignment
- 2015
- pp
- Collisions
