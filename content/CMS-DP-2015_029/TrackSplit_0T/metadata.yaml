title: 'Cosmic track-splitting validation on 2015 cosmic ray data at 0T (from interfill runs during 0T collisions)'

caption: 'Cosmic ray tracks are split in half at the hit closest to origin and refitted with the alignment constants under consideration. The differences in various track parameters between the two half-tracks are studied. The width of the distribution measures the achieved alignment precision, while deviations from zero indicate possible biases.
Shown are the normalized differences between two halves of a cosmic track, split at the point of closest approach to the interaction region: 

$\bullet$ in the xy distance between the track and the origin ($d_{xy}$)

$\bullet$ in the distance in the z direction between the track and the origin ($d_{z}$)

$\bullet$ in the track polar angle $\theta$

$\bullet$ in the track azimuthal angle $\phi$. 

The observed precision using the aligned geometry (black filled squares), produced with the Millepede-II and HIP algorithms using cosmic ray and 0T collision data, is a major improvement over the Run I geometry (blue empty squares) which is no longer valid for Run II data, primarily because of temperature changes and pixel re-centering and repair. In $d_{xy}$ and $d_{z}$, the precision is close to that of the ideal Monte Carlo, illustrating that the tracker has almost reached its spatial angular resolution for 0 tesla data. In the angular parameters, the precision is approximately the same as that of the ideal Monte Carlo, illustrating that the tracker has reached its design angular resolution for 0 tesla data. The small difference observed in the $\Delta\phi$ distribution is believed to be the result of a statistical fluctuation.
'

date: '2015/08/10'

tags:
- Run-2
- Alignment
- 2015
- Cosmics
- Magnet Off
