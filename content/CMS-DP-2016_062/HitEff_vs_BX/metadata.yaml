title: 'Hit efficiency as a function of the LHC bunch-crossing number for different LHC filling schemes'
caption: 'Hit efficiency as a function of the LHC bunch-crossing number for the innermost, second, and third barrel layers for different LHC filling schemes.

The hit efficiency is defined as the probability to find any clusters within a 500 micron radius around an expected hit. The goal is to quantify pure sensor efficiency with tracking effects maximally removed.

Each bin shows 60 consecutive bunch crossings in order to reduce statistical errors. The main souce of inefficiencies is due to dynamic data losses. The efficiency recovers to around 100% right after the abort gap (between BX 3300-3600) due to the empty readout buffers. Efficiency rapidly decreases after each gap between proton trains and reach a dynamic equilibrium after around the average trigger latency (~500 bx) when the rate of emptying and filling of buffers become similar. The innermost layer experiences the largest influx of particles per unit area.

Event selection:

$\bullet$ ZeroBias trigger

$\bullet$ At least 1 good primary vertex ($z < 25$ cm, $\rho < 2$ cm, $N_\text{d.o.f} > 4$)

Track selection:

$\bullet$ Well separated, good quality tracks with $p_\text{T} > 1$ GeV

$\bullet$ More than 10 hits in the strip detctor

$\bullet$ Associated to primary vertex with tight impact parameter cut

$\bullet$ Missing hit allowed only on layer under investigation (valid hits are expected on two „other” layers/disks)

Module selection:

$\bullet$ Bad read-outs removed

$\bullet$ ROCs under SEU (temporarily disfunctioning ROCs) removed

$\bullet$ ROC and module edges, as well as, overlap areas of adjacent modules within a layer rejected

$\bullet$ Only modules with good illumination by tracks are selected'
date: '2016/10/03'
tags:
- Run-2
- pp
- Collisions
- Phase-0 Pixel
- 2015
