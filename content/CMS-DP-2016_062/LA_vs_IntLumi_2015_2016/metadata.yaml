title: 'The Lorentz angle as a function of integrated luminosity in LHC Run 2'
caption: 'The Lorentz angle as a function of integrated luminosity in LHC Run 2 for old / new modules.

The measurement of the Lorentz angle for the barrel pixel is performed using the grazing angle technique, which uses well measured muon tracks to determine the drift of the electrons in pixel sensors. In order to measure the drift length as a function of depth, only tracks with shallow impact angle with respect to the local y axis are used, which corresponds to tracks with a high pseudorapidity.

The selection requirements include:

$\bullet$ muon tracks

$\bullet$ cluster size in $y \geq 4$

$\bullet$ cluster charge $<$ 120 ke

$\bullet$ track $p_\text{T} > 3$ GeV

$\bullet$ hit residuals $<$ 50 $\mu$m

$\bullet$ $\chi^2/N_\text{d.o.f} < 2$

$\bullet$ edge pixels excluded.

In Long Shutdown 1, damaged and non-functional modules have been replaced, since then the Lorentz Angle was measured separately for new and old modules.

The new modules behave very much like the old modules in the beginning of Run 1.

A rapidly increasing trend can be seen in the Lorentz Angle for the replaced innermost modules while a moderate trend is seen for the rest.'
date: '2016/10/03'
tags:
- Run-2
- pp
- Collisions
- Phase-0 Pixel
- Radiation
- Time Evolution
- 2015
- 2016
