title: 'The Lorentz angle as a function of bias voltage'
caption: 'The Lorentz angle, determined using the grazing angle method, is shown for several values of bias voltage.

The grazing angle method uses well measured muon tracks to determine the drift of the electrons in pixel sensors. In order to measure the drift length as a function of depth, only tracks with shallow impact angle with respect to the local y axis are used, which corresponds to tracks with a high pseudorapidity.

The selection requirements include:

$\bullet$ muon tracks

$\bullet$ cluster size in $y \geq 4$

$\bullet$ cluster charge $<$ 120 ke

$\bullet$ track $p_\text{T} > 3$ GeV

$\bullet$ hit residuals $<$ 50 $\mu$m

$\bullet$ $\chi^2/N_\text{d.o.f} < 2$

$\bullet$ edge pixels excluded.

Run 2 changed conditions:

$\bullet$ The Temparature of the Silicon Tracker was decreased from 0$^\circ$C to -10$^\circ$C

$\bullet$ The High Voltage Bias on the sensor was increased from 150 V to 200 V

Different voltage scan points were taken just before and after the Long Shutdown 1

$\bullet$ The Lorentz drift length depends on the applied voltage

The plot is based on:

$\bullet$ three special runs taken at the end of 2012 data taking period with the bias voltage of 140 V, 160 V and 300 V

$\bullet$ run # 207454, end of 2012 data taking period, Ubias = 150 V

$\bullet$ several high statistics runs from 2015D period, Ubias = 200 V'
date: '2016/10/03'
tags:
- Run-1
- Run-2
- pp
- Collisions
- Phase-0 Pixel
- 2012
- 2015
