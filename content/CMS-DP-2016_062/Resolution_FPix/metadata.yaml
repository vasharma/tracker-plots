title: 'FPix resolution as a function of integrated luminosity during 2015'
caption: 'Evolution of the forward pixel resolution along the direction $r$ perpendicular to beam axis (local $x$) with integrated luminosity delivered by the LHC, for prompt-reconstruction and final-reconstruction data. Comparison between the prompt- and final-reconstruction data using the template algorithm and between the generic and template algorithms for the final-reconstruction data are shown.

Tracks with $p_\text{T} > 4$ GeV having hits in both disks of the pixel forward detector and in the first layer of the pixel barrel detector are selected.

$\bullet$ The tracks are re-fit without the hit in the 1st endcap disk.

$\bullet$ Then, the residual difference between the hit position and the interpolated track is plotted.

$\bullet$ A student-t function is fit to the distribution.

$\bullet$ Assuming the resolution is the same in all three layers, the width of the function fit divided by $\sqrt{3/2}$ gives the intrinsic pixel resolution.

$\bullet$ This takes into account that the hit positions in layers 1 and 3 are smeared as well.

Positions are reconstructed with two algorithms: one based on track position and angle (Generic) and the other based on cluster shape simulations predicted by PIXELAV (Template).

The resolution mesurement is seen to depend slightly on the running conditions and improve with the best conditions (Final reconstruction). The template algorithm shows superior performance also for the forward pixels. Compared to the promptly reconstructed data, there was a large improvement seen on the first 7 points.'
date: '2016/10/03'
tags:
- Run-2
- pp
- Collisions
- Phase-0 Pixel
- Time Evolution
- 2015
