title: 'Hit efficiency as a function of instantaneous luminosity and average pileup'
caption: 'Hit efficiency as a function of instantaneous luminosity and the average number of inelastic proton-proton collisions (pileup) in 2016 in LHC filling schemes with more than 2000 colliding bunches.

The hit efficiency is defined as the probability to find any clusters within a 500 micron radius around an expected hit. The goal is to quantify pure sensor efficiency with tracking effects maximally removed.

The limited size of the internal buffer of the readout chips cause a dynamic inefficiency that increases with the instantaneous luminosity. This is an expected behaviour of the current readout chip design. SEU candidates, data aquisition problems and modules on the inner ring of Disk 1 were excluded.

Event selection:

$\bullet$ ZeroBias trigger

$\bullet$ At least 1 good primary vertex ($z < 25$ cm, $\rho < 2$ cm, $N_\text{d.o.f} > 4$)

Track selection:

$\bullet$ Well separated, good quality tracks with $p_\text{T} > 1$ GeV

$\bullet$ More than 10 hits in the strip detctor

$\bullet$ Associated to primary vertex with tight impact parameter cut

$\bullet$ Missing hit allowed only on layer under investigation (valid hits are expected on two „other” layers/disks)

Module selection:

$\bullet$ Bad read-outs removed

$\bullet$ ROCs under SEU (temporarily disfunctioning ROCs) removed

$\bullet$ ROC and module edges, as well as, overlap areas of adjacent modules within a layer rejected

$\bullet$ Only modules with good illumination by tracks are selected'

date: '2016/10/03'
tags:
- Run-2
- pp
- Collisions
- Phase-0 Pixel
- 2016
